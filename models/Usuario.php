<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $usuario
 * @property string $clave
 * @property int $estado
 */
class Usuario extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public $auth_key;
    public $primaryKey = "id_usuario";

    public static function tableName()
    {
        return 'PAISDB.equipo_informatico.usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['flg_activo'], 'integer'],
            [['usuario', 'clave'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }


    public function getUsername(){

        /* $datos_generales = (new \yii\db\Query())
                ->select('*')
                ->from('PAISDB.equipo_informatico.usuario')
                ->where('usuario=:usuario',[':usuario'=>$this->usuario])
                ->one(); */
        return $this->usuario;
    }

    public static function findByUsername($password,$username)
    {
      return static::find()->where('usuario=:usuario and flg_activo=1',[':usuario' => $username])->one();
    }


    public function validatePassword($password,$username){

        
        $model=static::find()->where('usuario=:usuario and flg_activo=1',[':usuario' => $username])->one();
        
        if ($password == $model->clave) {
            return $model;
        }
        return false;
    }

    public static function findIdentityByAccessToken($token, $type = null){
        if ($user['accessToken'] === $token) {
           return new static($user);
        }
        return null;
    }

    public function getAuthKey(){
        return $this->auth_key;
    }

    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }
}
