<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipo_informatico".
 *
 * @property int $id_equipo_informatico
 * @property int|null $id_tipo_equipo_informatico
 * @property string|null $descripcion_equipo_informatico
 * @property string|null $codigo_patrimonial
 * @property int|null $id_modelo
 * @property string|null $color
 * @property string|null $serie
 * @property string|null $fec_ingreso
 * @property string|null $proveedor_garantia
 * @property string|null $fec_fin_garantia_proveedor
 * @property string|null $proveedor_telefono
 * @property int|null $flg_activo
 * @property int|null $id_usuario_reg
 * @property string|null $fecha_reg
 * @property string|null $ipmaq_reg
 * @property int|null $id_usuario_act
 * @property string|null $fecha_act
 * @property string|null $ipmaq_act
 * @property int|null $id_usuario_del
 * @property string|null $fecha_del
 * @property string|null $ipmaq_del
 */
class EquipoInformaticoAsignacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'PAISDB.equipo_informatico.equipo_informatico_asignacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_equipo_informatico', 'id_usuario_asignado', 'id_unidad_territorial', 'flg_activo', 'fecha_asignacion'], 'safe'],
        ];
    }

}
