<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipo_informatico".
 *
 * @property int $id_equipo_informatico
 * @property int|null $id_tipo_equipo_informatico
 * @property string|null $descripcion_equipo_informatico
 * @property string|null $codigo_patrimonial
 * @property int|null $id_modelo
 * @property string|null $color
 * @property string|null $serie
 * @property string|null $fec_ingreso
 * @property string|null $proveedor_garantia
 * @property string|null $fec_fin_garantia_proveedor
 * @property string|null $proveedor_telefono
 * @property int|null $flg_activo
 * @property int|null $id_usuario_reg
 * @property string|null $fecha_reg
 * @property string|null $ipmaq_reg
 * @property int|null $id_usuario_act
 * @property string|null $fecha_act
 * @property string|null $ipmaq_act
 * @property int|null $id_usuario_del
 * @property string|null $fecha_del
 * @property string|null $ipmaq_del
 */
class EquipoInformatico extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'PAISDB.equipo_informatico.equipo_informatico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipo_equipo_informatico', 'id_modelo', 'flg_activo', 'id_usuario_reg', 'id_usuario_act', 'id_usuario_del'], 'integer'],
            [['fec_ingreso', 'fec_fin_garantia_proveedor', 'fecha_reg', 'fecha_act', 'fecha_del'], 'safe'],
            [['descripcion_equipo_informatico', 'proveedor_garantia'], 'string', 'max' => 150],
            [['codigo_patrimonial', 'color', 'serie'], 'string', 'max' => 25],
            [['proveedor_telefono'], 'string', 'max' => 12],
            [['ipmaq_reg', 'ipmaq_act', 'ipmaq_del'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_equipo_informatico' => 'Id Equipo Informatico',
            'id_tipo_equipo_informatico' => 'Id Tipo Equipo Informatico',
            'descripcion_equipo_informatico' => 'Descripcion Equipo Informatico',
            'codigo_patrimonial' => 'Codigo Patrimonial',
            'id_modelo' => 'Id Modelo',
            'color' => 'Color',
            'serie' => 'Serie',
            'fec_ingreso' => 'Fec Ingreso',
            'proveedor_garantia' => 'Proveedor Garantia',
            'fec_fin_garantia_proveedor' => 'Fec Fin Garantia Proveedor',
            'proveedor_telefono' => 'Proveedor Telefono',
            'flg_activo' => 'Flg Activo',
            'id_usuario_reg' => 'Id Usuario Reg',
            'fecha_reg' => 'Fecha Reg',
            'ipmaq_reg' => 'Ipmaq Reg',
            'id_usuario_act' => 'Id Usuario Act',
            'fecha_act' => 'Fecha Act',
            'ipmaq_act' => 'Ipmaq Act',
            'id_usuario_del' => 'Id Usuario Del',
            'fecha_del' => 'Fecha Del',
            'ipmaq_del' => 'Ipmaq Del',
        ];
    }
}
