<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipo_informatico".
 *
 * @property int $id_equipo_informatico
 * @property int|null $id_tipo_equipo_informatico
 * @property string|null $descripcion_equipo_informatico
 * @property string|null $codigo_patrimonial
 * @property int|null $id_modelo
 * @property string|null $color
 * @property string|null $serie
 * @property string|null $fec_ingreso
 * @property string|null $proveedor_garantia
 * @property string|null $fec_fin_garantia_proveedor
 * @property string|null $proveedor_telefono
 * @property int|null $flg_activo
 * @property int|null $id_usuario_reg
 * @property string|null $fecha_reg
 * @property string|null $ipmaq_reg
 * @property int|null $id_usuario_act
 * @property string|null $fecha_act
 * @property string|null $ipmaq_act
 * @property int|null $id_usuario_del
 * @property string|null $fecha_del
 * @property string|null $ipmaq_del
 */
class ProgramacionPreventivaEquipoInformatico extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'PAISDB.equipo_informatico.programacion_preventiva_equipo_informatico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fec_ingreso', 'fec_fin_garantia_proveedor', 'fecha_reg', 'fecha_act', 'fecha_del'], 'safe'],
        ];
    }

}
