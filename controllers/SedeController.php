<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class SedeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo "No ingresar";
        return false;
    }

    public function actionGetListaUnidadesTerritoriales(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $unidadesTerritoriales = [['id'=>1,'descripcion'=>'Unidad Territorial 1'],['id'=>2,'descripcion'=>'Unidad Territorial 2']];
            return ['success'=>true,'unidadesTerritoriales'=>$unidadesTerritoriales];
        }
    }

    public function actionGetListaUnidadesOrganicas(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $unidadesOrganicas = [['id'=>1,'descripcion'=>'Unidad Organica 1'],['id'=>2,'descripcion'=>'Unidad Organica 2']];
            return ['success'=>true,'unidadesOrganicas'=>$unidadesOrganicas];
        }
    }

    public function actionGetListaTambos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idUnidadTerritorial = $_POST['idUnidadTerritorial'];
            
            $tambos = [['id'=>1,'descripcion'=>'Tambo 1'],['id'=>2,'descripcion'=>'Tambo 2']];
            return ['success'=>true,'tambos'=>$tambos];
        }
    }

}
