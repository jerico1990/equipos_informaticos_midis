<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\EquipoInformaticoTicket;

class TicketController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo "No ingresar";
        return false;
    }

    public function actionCreate($idTicket,$idEquipoInformatico){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new EquipoInformaticoTicket();
        $model->titulo = 'Registrar ticket';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->flg_activo=1;
                $model->id_equipo_informatico = $idEquipoInformatico;
                $model->id_ticket = $idTicket;
                $model->id_usuario_reg = Yii::$app->user->id ;
                $model->fecha_reg = date('Y-m-d') ;
                $model->ipmaq_reg = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($idEquipoInformaticoTicket){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = EquipoInformaticoTicket::findOne($idEquipoInformaticoTicket);
        $model->titulo = 'Actualizar ticket';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->id_usuario_act = Yii::$app->user->id ;
                $model->fecha_act = date('Y-m-d') ;
                $model->ipmaq_act = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionGetListaTickets(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idEquipoInformatico = $_POST['idEquipoInformatico'];
            $tickets = (new \yii\db\Query())
                ->select("*")
                ->from('PAISDB.equipo_informatico.vw_lista_equipos_informaticos_ticket')
                ->where('id_equipo_informatico=:id_equipo_informatico',[':id_equipo_informatico'=>$idEquipoInformatico])
                ->all();
            return ['success'=>true,'tickets'=>$tickets];
        }
    }

    public function actionDelete(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id = $_POST['idEquipoInformaticoTicket'];
            $model = EquipoInformaticoTicket::findOne($id);
            /* $model->material = null;
            $model->repuesto = null;
            $model->id_usuario_del = Yii::$app->user->id ;
            $model->fecha_del = date('Y-m-d') ;
            $model->ipmaq_del = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null; */
            $model->delete();
            return ['success'=>true];
        }
    }

}
