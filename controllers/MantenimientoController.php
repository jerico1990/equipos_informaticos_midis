<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\EquipoInformaticoMantenimiento;
use yii\web\UploadedFile;

class MantenimientoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo "No ingresar";
        return false;
    }

    public function actionCreate($idEquipoInformatico){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new EquipoInformaticoMantenimiento();
        $model->titulo = 'Registrar mantenimiento';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->flg_activo=1;
                $model->id_equipo_informatico = $idEquipoInformatico;
                if($model->save()){
                    $model->archivo = UploadedFile::getInstance($model, 'archivo');
                    if($model->archivo){
                        $model->archivo->saveAs('mantenimientoArchivo/' . $model->id_equipo_informatico_mantenimiento . '.' . $model->archivo->extension);
                        $model->ruta_informe_tecnico = $model->id_equipo_informatico_mantenimiento . '.' . $model->archivo->extension;
                        $model->informe_tecnico = $model->archivo->name;
                        $model->id_usuario_reg = Yii::$app->user->id ;
                        $model->fecha_reg = date('Y-m-d') ;
                        $model->ipmaq_reg = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
                        $model->update();
                    }

                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = EquipoInformaticoMantenimiento::findOne($id);
        $model->titulo = 'Actualizar mantenimiento';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    $model->archivo = UploadedFile::getInstance($model, 'archivo');
                    if($model->archivo){
                        $model->archivo->saveAs('mantenimientoArchivo/' . $model->id_equipo_informatico_mantenimiento . '.' . $model->archivo->extension);
                        $model->ruta_informe_tecnico = $model->id_equipo_informatico_mantenimiento . '.' . $model->archivo->extension;
                        $model->informe_tecnico = $model->archivo->name;
                        $model->id_usuario_act = Yii::$app->user->id ;
                        $model->fecha_act = date('Y-m-d') ;
                        $model->ipmaq_act = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
                        $model->update();
                    }
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionGetListaMantenimientos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idEquipoInformatico = $_POST['idEquipoInformatico'];

            $mantenimientos = (new \yii\db\Query())
                ->select("*")
                ->from('PAISDB.equipo_informatico.vw_lista_equipos_informaticos_mantenimientos')
                ->where('id_equipo_informatico=:id_equipo_informatico',[':id_equipo_informatico'=>$idEquipoInformatico])
                ->all();
            return ['success'=>true,'mantenimientos'=>$mantenimientos];
        }
    }

    public function actionDelete(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $id = $_POST['id'];
            $model = EquipoInformaticoMantenimiento::findOne($id);
            $model->id_usuario_del = Yii::$app->user->id ;
            $model->fecha_del = date('Y-m-d') ;
            $model->ipmaq_del = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
            $model->update();
            return ['success'=>true];
        }
    }

}
