<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\EquipoInformatico;

class ProgramacionPreventivaEquipoInformaticoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo "No ingresar";
        return false;
    }

    public function actionGetListaProgramaciones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $programacionesPreventivas = (new \yii\db\Query())
                ->select("*")
                ->from('PAISDB.equipo_informatico.programacion_preventiva_equipo_informatico')
                ->all();
            return ['success'=>true,'programacionesPreventivas'=>$programacionesPreventivas];
        }
    }

}
