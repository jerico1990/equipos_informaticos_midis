<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\EquipoInformatico;

class EquipoInformaticoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        $this->layout='privado';
        return $this->render('index');
    }

    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new EquipoInformatico();
        $model->titulo = 'Registrar equipo informatico';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $equipoInformatico = EquipoInformatico::find()->where('codigo_patrimonial=:codigo_patrimonial',[':codigo_patrimonial'=>$model->codigo_patrimonial])->one();
                if($equipoInformatico){
                    return ['success'=>false,'msg'=>1];
                }
                //$model->int_estado = 1 ;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false,'msg'=>0];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = EquipoInformatico::findOne($id);
        $model->titulo = 'Actualizar equipo informatico';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionDetalleEquipoInformatico($id=null){
        $this->layout='privado';
        return $this->render('detalle-equipo-informatico',['id'=>$id]);
    }


    public function actionGetListaEquiposInformaticos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $equiposInformaticos = (new \yii\db\Query())
                ->select("*")
                ->from('PAISDB.equipo_informatico.vw_lista_equipos_informaticos')
                ->all();
            return ['success'=>true,'equiposInformaticos'=>$equiposInformaticos];
        }
    }

    public function actionGetInformacionGeneral(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idEquipoInformatico = $_POST['idEquipoInformatico'];

            $informacionGeneral = (new \yii\db\Query())
                ->select("*")
                ->from('PAISDB.equipo_informatico.vw_lista_equipos_informaticos')
                ->where('id_equipo_informatico=:id_equipo_informatico',[':id_equipo_informatico'=>$idEquipoInformatico])
                ->one();

            return ['success'=>true,'informacionGeneral'=>$informacionGeneral]; 
        }
    }

}
