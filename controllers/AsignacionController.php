<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\EquipoInformaticoAsignacion;

class AsignacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo "No ingresar";
        return false;
    }

    public function actionCreate($idEquipoInformatico){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new EquipoInformaticoAsignacion();
        $model->titulo = 'Registrar asignación';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->flg_activo=1;
                $model->id_equipo_informatico = $idEquipoInformatico;
                /* if($model->flg_ubicacion_diferente=="on"){
                    $model->flg_ubicacion_diferente="1";
                }else{
                    $model->flg_ubicacion_diferente=null;
                } */
                if($model->flg_activo=="1"){
                    EquipoInformaticoAsignacion::updateAll(['flg_activo' => '2'], ['=', 'id_equipo_informatico', $idEquipoInformatico]);
                }

                /*
                $model->id_ticket = $idTicket;
                $model->id_usuario_reg = Yii::$app->user->id ;
                $model->fecha_reg = date('Y-m-d') ;
                $model->ipmaq_reg = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null; */

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = EquipoInformaticoAsignacion::findOne($id);
        $model->titulo = 'Actualizar asignación';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                /* $model->id_usuario_act = Yii::$app->user->id ;
                $model->fecha_act = date('Y-m-d') ;
                $model->ipmaq_act = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null; */
                /* if($model->flg_ubicacion_diferente=="on"){
                    $model->flg_ubicacion_diferente="1";
                }else{
                    $model->flg_ubicacion_diferente=null;
                } */
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionGetListaAsignaciones(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idEquipoInformatico = $_POST['idEquipoInformatico'];

            $asignaciones = (new \yii\db\Query())
                ->select("*")
                ->from('PAISDB.equipo_informatico.vw_lista_equipos_informaticos_asignaciones')
                ->where('id_equipo_informatico=:id_equipo_informatico',[':id_equipo_informatico'=>$idEquipoInformatico])
                ->orderBy('id_equipo_informatico_asignacion desc')
                ->all();
            return ['success'=>true,'asignaciones'=>$asignaciones];
        }
    }

}
