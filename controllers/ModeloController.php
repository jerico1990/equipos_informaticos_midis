<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Modelo;

class ModeloController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo "No ingresar";
        return false;
    }
/* 
    public function actionCreate(){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = new EquipoInformatico();
        $model->titulo = 'Registrar equipo informatico';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                //$model->int_estado = 1 ;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = EquipoInformatico::findOne($id);
        $model->titulo = 'Actualizar equipo informatico';
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    } */


    public function actionGetListaModelos(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idMarca = $_POST['idMarca'];

            $modelos = (new \yii\db\Query())
                ->select('*')
                ->from('PAISDB.equipo_informatico.modelo')
                ->where('id_marca=:id_marca and flg_activo=1',[':id_marca'=>$idMarca])
                ->all();
            return ['success'=>true,'modelos'=>$modelos];
        }
    }

}
