<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\EquipoInformatico;

class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        echo "No ingresar";
        return false;
    }

    public function actionGetListaResponsables(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $responsables = (new \yii\db\Query())
                ->select('*')
                ->from('PAISDB.equipo_informatico.vw_lista_personal_soporte')
                ->all();


            //$responsables = [['id'=>1,'nombres'=>'Cesar Gago'],['id'=>2,'nombres'=>'Jorge']];
            return ['success'=>true,'responsables'=>$responsables];
        }
    }


    public function actionGetListaResponsablesAsignacion(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idUbicacion = $_POST['idUbicacion'];
            $responsables = (new \yii\db\Query())
                ->select('*')
                ->from('PAISDB.equipo_informatico.vw_lista_usuarios')
                ->where('id_ubicacion=:id_ubicacion',[':id_ubicacion'=>$idUbicacion])
                ->all();


            //$responsables = [['id'=>1,'nombres'=>'Cesar Gago'],['id'=>2,'nombres'=>'Jorge']];
            return ['success'=>true,'responsables'=>$responsables];
        }
    }

    public function actionGetListaUsuarios(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $idTipoSede = $_POST['idTipoSede'];
            $idPadre = $_POST['idPadre'];
            if($idTipoSede=="1"){
                $usuarios = [['id'=>1,'nombres'=>'Cesar Gago - Unidad Organica'],['id'=>2,'nombres'=>'Jorge - Unidad Organica']];
            }else if($idTipoSede=="2"){
                $usuarios = [['id'=>1,'nombres'=>'Cesar Gago - Tambo'],['id'=>2,'nombres'=>'Jorge - Tambo']];
            }else if($idTipoSede=="3"){
                $usuarios = [['id'=>1,'nombres'=>'Cesar Gago - Unidad Territorial'],['id'=>2,'nombres'=>'Jorge - Unidad Territorial']];
            }

            
            return ['success'=>true,'usuarios'=>$usuarios];
        }
    }

}
