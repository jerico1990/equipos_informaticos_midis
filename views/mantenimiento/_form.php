<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formEquipoInformaticoMantenimiento','class' => 'form-horizontal']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Fecha de mantenimiento</label>
                    <input type="date" id="equipo-informatico-mantenimiento-fecha_mantenimiento" class="form-control" name="EquipoInformaticoMantenimiento[fecha_mantenimiento]" value="<?= $model->fecha_mantenimiento ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tipo de mantenimiento:</label>
                    <select id="equipo-informatico-mantenimiento-id_tipo_mantenimiento" class="form-control" name="EquipoInformaticoMantenimiento[id_tipo_mantenimiento]">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Responsable:</label>
                    <select id="equipo-informatico-mantenimiento-id_usuario_responsable" class="form-control" name="EquipoInformaticoMantenimiento[id_usuario_responsable]">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Informe técnico:</label>
                    <input type="file" id="equipo-informatico-mantenimiento-archivo" class="form-control" name="EquipoInformaticoMantenimiento[archivo]">
                    <div class="archivo-mantenimiento" style="display:none">
                        <a href="<?= \Yii::$app->request->BaseUrl ?>/mantenimientoArchivo/<?= $model->ruta_informe_tecnico ?>" class="btn btn-success">Descargar</a> <span class="btn btn-danger btn-eliminar-archivo-mantenimiento">X</span>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Descripcion:</label>
                    <textarea  id="equipo-informatico-mantenimiento-observacion" class="form-control" name="EquipoInformaticoMantenimiento[observacion]" cols="30" rows="5"><?= $model->observacion ?></textarea>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success btn-grabar-mantenimiento">Grabar</button>
    </div>
<?php ActiveForm::end(); ?>