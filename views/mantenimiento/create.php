<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */

?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>
