<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
?>

<div class="modal fade" style="z-index:1060" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog text-center" role="document">
            Cargando <br>
            <div class="spinner-border text-primary m-1" role="status"></div>
        </div>
    </div>

<div class="account-pages my-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card overflow-hidden">
                    <div class="bg-success">
                        <div class="row">
                            <div class="col-9">
                                <div class="text-white p-4">
                                    <h5 class="text-white">MIDIS</h5>
                                    <p>Ingrese su sesión</p>
                                </div>
                            </div>
                            <div class="col-3 align-self-end">
                                <!-- <img src="<?= \Yii::$app->request->BaseUrl ?>/skote/assets/images/profile-img.png" alt="" class="img-fluid"> -->
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <!-- <div>
                            <a href="#">
                                <div class="avatar-md profile-user-wid mb-4">
                                    <span class="avatar-title rounded-circle bg-success">
                                        <img src="<?= \Yii::$app->request->BaseUrl ?>/img/midagri_login.jpg" alt="" class="rounded-circle" height="54">
                                    </span>
                                </div>
                            </a>
                        </div> -->
                        <div class="p-2">
                            <?php $form = ActiveForm::begin(); ?>

                            <div class="form-group">
                                <label for="username">Usuario</label>
                                <input type="text" class="form-control" id="username" name="LoginForm[username]" placeholder="Ingresa usuario">
                            </div>

                            <div class="form-group">
                                <label for="userpassword">Clave</label>
                                <input type="password" class="form-control" id="password" name="LoginForm[password]" placeholder="Ingresa clave">
                            </div>

                            <div class="form-group">
                                <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Ingresar</button>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>

                    </div>
                </div>
                <div class="mt-5 text-center">
                    <div>
                        <p>© 2022 - MIDIS | PERÚ. </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
