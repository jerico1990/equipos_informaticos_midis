
<!-- start page title -->
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Lista de equipos informaticos</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Index</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">&nbsp&nbsp</label> <br>
                            <button class="btn btn-success btn-agregar-equipo-informatico">Agregar</button>
                        </div>
                    </div>
                </div>
                <table id="lista-equipos-informaticos" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Código Patrimonial</th>
                            <th>Denominación</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Estado</th>
                            <th>Ubicacion</th>
                            <th>Responsabe actual</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>



<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');


$('#lista-equipos-informaticos').DataTable();
EquiposInformaticos();
async function EquiposInformaticos(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/equipo-informatico/get-lista-equipos-informaticos',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var equiposInformaticos ="";
                        $('#lista-equipos-informaticos').DataTable().destroy();
                        $.each(results.equiposInformaticos, function( index, value ) {
                            var flg_activo="";
                            if(value.flg_activo==1){
                                flg_activo = "Activo";
                            }

                            equiposInformaticos = equiposInformaticos + "<tr>";
                                equiposInformaticos = equiposInformaticos + "<td>" + ((value.codigo_patrimonial)?value.codigo_patrimonial:"") + "</td>";
                                equiposInformaticos = equiposInformaticos + "<td>" + ((value.descripcion_equipo_informatico)?value.descripcion_equipo_informatico:"") + "</td>";
                                equiposInformaticos = equiposInformaticos + "<td>" + ((value.descripcion_marca)?value.descripcion_marca:"") + "</td>";
                                equiposInformaticos = equiposInformaticos + "<td>" + ((value.descripcion_modelo)?value.descripcion_modelo:"") + "</td>";
                                equiposInformaticos = equiposInformaticos + "<td>" + ((flg_activo)?flg_activo:"") + "</td>";
                                equiposInformaticos = equiposInformaticos + "<td>" + ((value.empleado)?value.empleado:"") + "</td>";
                                equiposInformaticos = equiposInformaticos + "<td>" + ((value.ubicacion)?value.ubicacion:"") + "</td>";
                                equiposInformaticos = equiposInformaticos + "<td>";
                                    equiposInformaticos = equiposInformaticos + '<button data-id="' + value.id_equipo_informatico + '" data-idMarca="' + value.id_marca + '" data-idModelo="' + value.id_modelo + '"  data-idTipoEquipoInformatico="' + value.id_tipo_equipo_informatico + '" class="btn btn-info btn-sm btn-modificar-equipo-informatico" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    equiposInformaticos = equiposInformaticos + '<a href="<?= \Yii::$app->request->BaseUrl ?>/equipo-informatico/detalle-equipo-informatico?id='+ value.id_equipo_informatico +'" class="btn btn-sm btn-primary" > <i class="fas fa-pen-square"></i> </a> ';
                                    equiposInformaticos = equiposInformaticos + '<button data-id="' + value.id_equipo_informatico + '" class="btn btn-danger btn-sm btn-eliminar-equipo-informatico" href="#"><i class="fas fa-trash"></i></button>';

                                    
                                equiposInformaticos = equiposInformaticos +"</td>";
                            equiposInformaticos = equiposInformaticos + "</tr>";
                        });
                        
                        $('#lista-equipos-informaticos tbody').html(equiposInformaticos);
                        $('#lista-equipos-informaticos').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal('hide'); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}



//agregar

$('body').on('click', '.btn-agregar-equipo-informatico', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/equipo-informatico/create',function(){
        Marcas();
        TipoEquipoInformatico();
    });
    $('#modal').modal('show');
});

//modificar
$('body').on('click', '.btn-modificar-equipo-informatico', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var idMarca = $(this).attr('data-idMarca');
    var idModelo = $(this).attr('data-idModelo');
    var idTipoEquipoInformatico = $(this).attr('data-idTipoEquipoInformatico');

    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/equipo-informatico/update?id='+id,function(){
        Marcas(idMarca);
        Modelos(idMarca,idModelo)
        TipoEquipoInformatico(idTipoEquipoInformatico);
    });
    $('#modal').modal('show');
});


//grabar

$('body').on('click', '.btn-grabar-equipo-informatico', function (e) {

    e.preventDefault();
    var form = $('#formEquipoInformatico');
    var formData = $('#formEquipoInformatico').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            //loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                EquiposInformaticos();
                $('#modal').modal('hide');
            }else{
                if(results.msg=="1"){
                    alert("El código patrimonial ya se encuentra registrado");
                    //loading.modal("hide");
                }
            }
        },
    });
});

async function TipoEquipoInformatico(idTipoEquipoInformatico){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/tipo-equipo-informatico/get-lista-tipos-equipo-informatico",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionstipoEquipoInformatico = "<option value>Seleccionar</option>";

                        $.each(results.tiposEquipoInformatico, function( index, value ) {
                            optionstipoEquipoInformatico = optionstipoEquipoInformatico + `<option value='${value.id_tipo_equipo_informatico}'>${value.descripcion_tipo_equipo_informatico}</option>`;
                        });

                        $("#equipo-informatico-id_tipo_equipo_informatico").html(optionstipoEquipoInformatico);

                        if(idTipoEquipoInformatico){
                            $("#equipo-informatico-id_tipo_equipo_informatico").val(idTipoEquipoInformatico);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function Marcas(idMarca){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/marca/get-lista-marcas",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsMarca = "<option value>Seleccionar</option>";

                        $.each(results.marcas, function( index, value ) {
                            optionsMarca = optionsMarca + `<option value='${value.id_marca}'>${value.descripcion_marca}</option>`;
                        });

                        $("#equipo-informatico-id_marca").html(optionsMarca);

                        if(idMarca){
                            $("#equipo-informatico-id_marca").val(idMarca);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function Modelos(idMarca,idModelo){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/modelo/get-lista-modelos",
                method: "POST",
                data:{_csrf:csrf,'idMarca':idMarca},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsModelo = "<option value>Seleccionar</option>";

                        $.each(results.modelos, function( index, value ) {
                            optionsModelo = optionsModelo + `<option value='${value.id_modelo}'>${value.descripcion_modelo}</option>`;
                        });

                        $("#equipo-informatico-id_modelo").html(optionsModelo);

                        if(idModelo){
                            $("#equipo-informatico-id_modelo").val(idModelo);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

$('body').on('change', '#equipo-informatico-id_marca', function (e) {
    e.preventDefault();
    idMarca = $(this).val();
    Modelos(idMarca);
});

</script>
