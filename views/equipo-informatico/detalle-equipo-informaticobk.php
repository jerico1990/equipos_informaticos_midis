
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Equipo Informatico</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Detalle</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Estado:</label> xx
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Código patrimonial:</label> xx
                    </div>
                    <div class="col-md-6">
                        <label for="">Denominación:</label> xx
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Fecha de ingreso:</label> xx
                    </div>
                    <div class="col-md-6">
                        <label for="">Tipo:</label> xx
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Marca:</label> xx
                    </div>
                    <div class="col-md-6">
                        <label for="">Modelo:</label> xx
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Color:</label> xx
                    </div>
                    <div class="col-md-6">
                        <label for="">Nro serie:</label> xx
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Lista de mantenimientos</b>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="button" class="btn btn-success btn-agregar-mantenimiento float-right">Agregar</button>
                        </div>
                    </div>
                </div>
                <hr>
                <table id="lista-mantenimientos" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Tipo</th>
                            <th>Descripción</th>
                            <th>Responsable</th>
                            <th>Informe</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Lista de Programaciones</b>
                        </div>
                    </div>
                </div>
                <hr>
                <table id="lista-programaciones" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Fecha de programación</th>
                            <th>Se ejecuto</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Lista de tickets</b>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button class="btn btn-success btn-agregar-extension-ticket float-right">Agregar</button>
                        </div>
                    </div>
                </div>
                <hr>
                <table id="lista-tickets" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Tickets</th>
                            <th>Fecha de creación</th>
                            <th>Asignado</th>
                            <th>Materiales</th>
                            <th>Repuestos</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Lista de asignaciones</b>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button class="btn btn-success btn-agregar-asignacion float-right">Agregar</button>
                        </div>
                    </div>
                </div>
                <hr>
                <table id="lista-asignaciones" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Fecha de asignacion</th>
                            <th>Tipo sede</th>
                            <th>Ubicación</th>
                            <th>Usuario</th>
                            <th>Ubicación diferente</th>
                            <th>Tipo sede</th>
                            <th>Ubicación</th>
                            <th>Usuario</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>
<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var idEquipoInformatico = "<?= $id ?>";

$('#lista-mantenimientos').DataTable();
$('#lista-programaciones').DataTable();
$('#lista-tickets').DataTable();
$('#lista-asignaciones').DataTable();

ListaMantenimientos();
ListaProgramaciones();
ListaTickets();
ListaAsignaciones();

async function ListaMantenimientos(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/mantenimiento/get-lista-mantenimientos',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var mantenimientos ="";
                        $('#lista-mantenimientos').DataTable().destroy();
                        $.each(results.mantenimientos, function( index, value ) {
                            
                            mantenimientos = mantenimientos + "<tr>";
                                mantenimientos = mantenimientos + "<td>" + ((value.fecha_mantenimiento)?value.fecha_mantenimiento:"") + "</td>";
                                mantenimientos = mantenimientos + "<td>" + ((value.descripcion_tipo_mantenimiento)?value.descripcion_tipo_mantenimiento:"") + "</td>";
                                mantenimientos = mantenimientos + "<td>" + ((value.observacion)?value.observacion:"") + "</td>";
                                mantenimientos = mantenimientos + "<td>" + ((value.id_usuario_responsable)?value.id_usuario_responsable:"") + "</td>";
                                mantenimientos = mantenimientos + "<td>" + ((value.informe_tecnico)?"<a target='_blank' class='btn btn-success' href='<?= \Yii::$app->request->BaseUrl ?>/mantenimientoArchivo/" + value.ruta_informe_tecnico + "'>Descargar</a>":"") + "</td>";

                                mantenimientos = mantenimientos + "<td>";
                                    mantenimientos = mantenimientos + '<button data-id="' + value.id_equipo_informatico_mantenimiento + '" data-idResponsable="' + value.id_usuario_responsable + '" data-idTipoMantenimiento="' + value.id_tipo_mantenimiento + '" data-rutaInformeTecnico="' + value.ruta_informe_tecnico + '" class="btn btn-info btn-sm btn-modificar-mantenimiento" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    mantenimientos = mantenimientos + '<button data-id="' + value.id_equipo_informatico_mantenimiento + '" class="btn btn-danger btn-sm btn-eliminar-mantenimiento" href="#"><i class="fas fa-trash"></i></button>';

                                mantenimientos = mantenimientos +"</td>";
                            mantenimientos = mantenimientos + "</tr>";
                        });
                        
                        $('#lista-mantenimientos tbody').html(mantenimientos);
                        $('#lista-mantenimientos').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal('hide'); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function ListaProgramaciones(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/programacion-preventiva-equipo-informatico/get-lista-programaciones',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var programacionesPreventivas ="";
                        $('#lista-programaciones').DataTable().destroy();
                        $.each(results.programacionesPreventivas, function( index, value ) {
                            
                            programacionesPreventivas = programacionesPreventivas + "<tr>";
                                programacionesPreventivas = programacionesPreventivas + "<td>" + ((value.fecha_programacion)?value.fecha_programacion:"") + "</td>";
                                programacionesPreventivas = programacionesPreventivas + "<td>" + ((value.flg_ejecuto_revision)?value.flg_ejecuto_revision:"") + "</td>";

                                programacionesPreventivas = programacionesPreventivas + "<td>";
                                    programacionesPreventivas = programacionesPreventivas + '<button data-id="' + value.id_programacion_preventiva_detalle + '" class="btn btn-info btn-sm btn-modificar-programacion" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    programacionesPreventivas = programacionesPreventivas + '<button data-id="' + value.id_programacion_preventiva_detalle + '" class="btn btn-danger btn-sm btn-eliminar-programacion" href="#"><i class="fas fa-trash"></i></button>';

                                    
                                programacionesPreventivas = programacionesPreventivas +"</td>";
                            programacionesPreventivas = programacionesPreventivas + "</tr>";
                        });
                        
                        $('#lista-programaciones tbody').html(programacionesPreventivas);
                        $('#lista-programaciones').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal('hide'); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function ListaTickets(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/ticket/get-lista-tickets',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var tickets ="";
                        $('#lista-tickets').DataTable().destroy();
                        $.each(results.tickets, function( index, value ) {
                            
                            tickets = tickets + "<tr>";
                                tickets = tickets + "<td>" + ((value.id_ticket)?value.id_ticket:"") + "</td>";
                                tickets = tickets + "<td>" + ((value.fecha_reg)?value.fecha_reg:"") + "</td>";
                                tickets = tickets + "<td>xxxxx</td>";  
                                tickets = tickets + "<td>" + ((value.material)?value.material:"") + "</td>";  
                                tickets = tickets + "<td>" + ((value.repuesto)?value.repuesto:"") + "</td>";  

                                tickets = tickets + "<td>";
                                    tickets = tickets + '<button data-id="' + value.id_equipo_informatico_ticket + '"  class="btn btn-info btn-sm btn-modificar-ticket" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    tickets = tickets + '<button data-id="' + value.id_equipo_informatico_ticket + '" class="btn btn-danger btn-sm btn-eliminar-ticket" href="#"><i class="fas fa-trash"></i></button>';

                                tickets = tickets +"</td>";
                            tickets = tickets + "</tr>";
                        });
                        
                        $('#lista-tickets tbody').html(tickets);
                        $('#lista-tickets').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal('hide'); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function ListaAsignaciones(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/asignacion/get-lista-asignaciones',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var asignaciones ="";
                        $('#lista-asignaciones').DataTable().destroy();
                        $.each(results.asignaciones, function( index, value ) {
                            var tipoSede = "";
                            var ubicacion = "";
                            if(value.id_tipo_sede=="1"){
                                tipoSede="Sede Central";
                                ubicacion="Unidad Organica/Lista de Usuarios";
                            }else if(value.id_tipo_sede=="2"){
                                tipoSede="Tambo";
                                ubicacion="Unidad Territorial/Tambos/Lista de Usuarios";
                            }else if(value.id_tipo_sede=="3"){
                                tipoSede="Unidad Territorial";
                                ubicacion="Unidad Territorial/Lista de Usuarios";
                            }

                            var tipoSedeDiferente = "";
                            var ubicacionDiferente = "";
                            if(value.id_tipo_sede_ubicacion_diferente=="1"){
                                tipoSedeDiferente="Sede Central";
                                ubicacionDiferente="Unidad Organica/Lista de Usuarios";
                            }else if(value.id_tipo_sede_ubicacion_diferente=="2"){
                                tipoSedeDiferente="Tambo";
                                ubicacionDiferente="Unidad Territorial/Tambos/Lista de Usuarios";
                            }else if(value.id_tipo_sede_ubicacion_diferente=="3"){
                                tipoSedeDiferente="Unidad Territorial";
                                ubicacionDiferente="Unidad Territorial/Lista de Usuarios";
                            }


                            asignaciones = asignaciones + "<tr>";
                                asignaciones = asignaciones + "<td>" + ((value.fecha_asignacion)?value.fecha_asignacion:"") + "</td>";
                                asignaciones = asignaciones + "<td>" + ((value.id_tipo_sede)?tipoSede:"") + "</td>";
                                asignaciones = asignaciones + "<td>" + ((value.id_tipo_sede)?ubicacion:"") + "</td>";
                                asignaciones = asignaciones + "<td>" + ((value.id_usuario_asignado)?value.id_usuario_asignado:"") + "</td>";

                                asignaciones = asignaciones + "<td>" + ((value.flg_ubicacion_diferente=="1")?"Si":"No") + "</td>";
                                asignaciones = asignaciones + "<td>" + ((value.id_tipo_sede_ubicacion_diferente)?tipoSedeDiferente:"") + "</td>";
                                asignaciones = asignaciones + "<td>" + ((value.id_tipo_sede_ubicacion_diferente)?ubicacionDiferente:"") + "</td>";
                                asignaciones = asignaciones + "<td>" + ((value.id_usuario_asignado_ubicacion_diferente)?value.id_usuario_asignado_ubicacion_diferente:"") + "</td>";

                                asignaciones = asignaciones + "<td>" + ((value.flg_activo=="1")?"Activo":"Desactivo") + "</td>";

                                asignaciones = asignaciones + "<td>";
                                    asignaciones = asignaciones + '<button data-id="' + value.id_equipo_informatico_asignacion + '" data-idTipoSede="' + value.id_tipo_sede + '" data-idUnidadOrganica="' + value.id_unidad_organica + '" data-idUnidadTerritorial="' + value.id_unidad_territorial + '" data-idTambo="' + value.id_tambo + '" data-idUsuarioAsignado="' + value.id_usuario_asignado + '" || data-idTipoSedeUbicacionDiferente="' + value.id_tipo_sede_ubicacion_diferente + '" data-idUnidadOrganicaUbicacionDiferente="' + value.id_unidad_organica_ubicacion_diferente + '" data-idUnidadTerritorialUbicacionDiferente="' + value.id_unidad_territorial_ubicacion_diferente + '" data-idTamboUbicacionDiferente="' + value.id_tambo_ubicacion_diferente + '" data-idUsuarioAsignadoUbicacionDiferente="' + value.id_usuario_asignado_ubicacion_diferente + '" data-flgUbicacionDiferente="' + value.flg_ubicacion_diferente + '" data-fechaAsignacion="' + value.fecha_asignacion + '" class="btn btn-info btn-sm btn-modificar-asignacion" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    asignaciones = asignaciones + '<button data-id="' + value.id_equipo_informatico_asignacion + '" class="btn btn-danger btn-sm btn-eliminar-asignacion" href="#"><i class="fas fa-trash"></i></button>';

                                    
                                asignaciones = asignaciones +"</td>";
                            asignaciones = asignaciones + "</tr>";
                        });
                        
                        $('#lista-asignaciones tbody').html(asignaciones);
                        $('#lista-asignaciones').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal('hide'); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

/* Mantenimiento */

async function MantenimientoResponsables(idResponsable){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/usuario/get-lista-responsables",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsResponsable = "<option value>Seleccionar</option>";

                        $.each(results.responsables, function( index, value ) {
                            optionsResponsable = optionsResponsable + `<option value='${value.id}'>${value.nombres}</option>`;
                        });

                        $("#equipo-informatico-mantenimiento-id_usuario_responsable").html(optionsResponsable);

                        if(idResponsable){
                            $("#equipo-informatico-mantenimiento-id_usuario_responsable").val(idResponsable);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function MantenimientoTipos(idTipoMantenimiento){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/tipo-mantenimiento/get-lista-tipos-mantenimiento",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsTiposMantenimiento = "<option value>Seleccionar</option>";

                        $.each(results.tiposMantenimiento, function( index, value ) {
                            optionsTiposMantenimiento = optionsTiposMantenimiento + `<option value='${value.id_tipo_mantenimiento}'>${value.descripcion_tipo_mantenimiento}</option>`;
                        });

                        $("#equipo-informatico-mantenimiento-id_tipo_mantenimiento").html(optionsTiposMantenimiento);

                        if(idTipoMantenimiento){
                            $("#equipo-informatico-mantenimiento-id_tipo_mantenimiento").val(idTipoMantenimiento);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

//agregar mantenimiento

$('body').on('click', '.btn-agregar-mantenimiento', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/mantenimiento/create?idEquipoInformatico='+idEquipoInformatico,function(){
        MantenimientoResponsables()
        MantenimientoTipos()
    });
    $('#modal').modal('show');
});

//modificar mantenimiento
$('body').on('click', '.btn-modificar-mantenimiento', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var idResponsable = $(this).attr('data-idResponsable');
    var idTipoMantenimiento = $(this).attr('data-idTipoMantenimiento');
    var rutaInformeTecnico = $(this).attr('data-rutaInformeTecnico');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/mantenimiento/update?id='+id,function(){
        MantenimientoResponsables(idResponsable);
        MantenimientoTipos(idTipoMantenimiento);
        if(rutaInformeTecnico){
            $('#equipo-informatico-mantenimiento-archivo').hide();
            $('.archivo-mantenimiento').show();
        }
    });
    $('#modal').modal('show');
});

$('body').on('click', '.btn-eliminar-mantenimiento', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/mantenimiento/delete",
        method: "POST",
        data:{_csrf:csrf,id:id},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                ListaMantenimientos()
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
});



$('body').on('click', '.btn-eliminar-archivo-mantenimiento', function (e) {
    e.preventDefault();
    $('#equipo-informatico-mantenimiento-archivo').show();
    $('.archivo-mantenimiento').hide();
});
//grabar mantenimiento

$('body').on('click', '.btn-grabar-mantenimiento', function (e) {

    e.preventDefault();
    var form = $('#formEquipoInformaticoMantenimiento');
    var formData = new FormData();
	formData.append("_csrf", csrf);
    formData.append("EquipoInformaticoMantenimiento[fecha_mantenimiento]", $('#equipo-informatico-mantenimiento-fecha_mantenimiento').val());
    formData.append("EquipoInformaticoMantenimiento[id_tipo_mantenimiento]", $('#equipo-informatico-mantenimiento-id_tipo_mantenimiento').val());
    formData.append("EquipoInformaticoMantenimiento[id_usuario_responsable]", $('#equipo-informatico-mantenimiento-id_usuario_responsable').val());
    formData.append("EquipoInformaticoMantenimiento[archivo]", document.getElementById('equipo-informatico-mantenimiento-archivo').files[0]);
    formData.append("EquipoInformaticoMantenimiento[observacion]", $('#equipo-informatico-mantenimiento-observacion').val());
 

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        cache: false,
		contentType: false,
		processData: false,
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                ListaMantenimientos();
                $('#modal').modal('hide');
            }
        },
    });
});

/* Ticket */

//agregar Ticket
$('body').on('click', '.btn-agregar-extension-ticket', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/ticket/create?idEquipoInformatico='+idEquipoInformatico+'&idTicket=1');
    $('#modal').modal('show');
});

//modificar mantenimiento
$('body').on('click', '.btn-modificar-ticket', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/ticket/update?id='+id);
    $('#modal').modal('show');
});


$('body').on('click', '.btn-eliminar-ticket', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/ticket/delete",
        method: "POST",
        data:{_csrf:csrf,id:id},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                ListaTickets()
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
});

//grabar Ticket

$('body').on('click', '.btn-grabar-ticket', function (e) {

    e.preventDefault();
    e.preventDefault();
    var form = $('#formEquipoInformaticoTicket');
    var formData = $('#formEquipoInformaticoTicket').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                ListaTickets();
                $('#modal').modal('hide');
            }
        },
    });
});


/* asingnacion */


$('body').on('click', '.btn-agregar-asignacion', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/asignacion/create?idEquipoInformatico='+idEquipoInformatico);
    $('#modal').modal('show');
});

$('body').on('change', '#id_tipo_sede', function (e) {
    e.preventDefault();
    var id_tipo_sede = $(this).val();
    $('.sede_central').hide();
    $('.tambo').hide();
    $('.unidad_territorial').hide();

    $('#id_unidad_territorial').val('');
    $('#id_unidad_organica').val('');
    $('#id_tambo').val('');
    $('#id_usuario_asignado').val('');

    if(id_tipo_sede=="1"){
        $('.sede_central').show();
        UnidadesOrganicas(null,"id_unidad_organica")
    }else if(id_tipo_sede=="2"){
        $('.tambo').show();
        UnidadesTerritoriales(null,"id_unidad_territorial");
    }else if(id_tipo_sede=="3"){
        $('.unidad_territorial').show();
        UnidadesTerritoriales(null,"id_unidad_territorial");
    }
});

$("body").on("change", "#flg_ubicacion_diferente", function (e) {
    e.preventDefault();
    $(".ubicacion_diferente").hide();
    $('.sede_central_ubicacion_diferente').hide();
    $('.tambo_ubicacion_diferente').hide();
    $('.unidad_territorial_ubicacion_diferente').hide();

    $('#id_tipo_sede_ubicacion_diferente').val('');

    $('#id_unidad_territorial_ubicacion_diferente').val('');
    $('#id_unidad_organica_ubicacion_diferente').val('');
    $('#id_tambo_ubicacion_diferente').val('');
    $('#id_usuario_asignado_ubicacion_diferente').val('');

    if($("[id=\"flg_ubicacion_diferente\"]:checked").val()){
        $(".ubicacion_diferente").show()
    }
});

$('body').on('change', '#id_tipo_sede_ubicacion_diferente', function (e) {
    e.preventDefault();
    var id_tipo_sede_ubicacion_diferente = $(this).val();
    $('.sede_central_ubicacion_diferente').hide();
    $('.tambo_ubicacion_diferente').hide();
    $('.unidad_territorial_ubicacion_diferente').hide();

    $('#id_unidad_territorial_ubicacion_diferente').val('');
    $('#id_unidad_organica_ubicacion_diferente').val('');
    $('#id_tambo_ubicacion_diferente').val('');
    $('#id_usuario_asignado_ubicacion_diferente').val('');

    if(id_tipo_sede_ubicacion_diferente=="1"){
        $('.sede_central_ubicacion_diferente').show();
        UnidadesOrganicas(null,"id_unidad_organica_ubicacion_diferente")
    }else if(id_tipo_sede_ubicacion_diferente=="2"){
        $('.tambo_ubicacion_diferente').show();
        UnidadesTerritoriales(null,"id_unidad_territorial_ubicacion_diferente");
    }else if(id_tipo_sede_ubicacion_diferente=="3"){
        $('.unidad_territorial_ubicacion_diferente').show();
        UnidadesTerritoriales(null,"id_unidad_territorial_ubicacion_diferente");
    }
});

$('body').on('change', '#id_unidad_organica', function (e) {
    e.preventDefault();
    var idUnidadOrganica = $(this).val();
    var idTipoSede = $('#id_tipo_sede').val();
    Usuarios(null,"id_usuario_asignado",idTipoSede,idUnidadOrganica);
});

$('body').on('change', '#id_unidad_territorial', function (e) {
    e.preventDefault();
    var idUnidadTerritorial = $(this).val();
    var idTipoSede = $('#id_tipo_sede').val();
    if(idTipoSede=="2"){
        Tambos(null,"id_tambo",idTipoSede,idUnidadTerritorial);
    }else if(idTipoSede=="3"){
        Usuarios(null,"id_usuario_asignado",idTipoSede,idUnidadTerritorial);
    }
});

$('body').on('change', '#id_tambo', function (e) {
    e.preventDefault();
    var idTambo = $(this).val();
    var idTipoSede = $('#id_tipo_sede').val();
    Usuarios(null,"id_usuario_asignado",idTipoSede,idTambo);
});



/* ubicacion diferente */

$('body').on('change', '#id_unidad_organica_ubicacion_diferente', function (e) {
    e.preventDefault();
    var idUnidadOrganica = $(this).val();
    var idTipoSede = $('#id_tipo_sede_ubicacion_diferente').val();
    Usuarios(null,"id_usuario_asignado_ubicacion_diferente",idTipoSede,idUnidadOrganica);
});

$('body').on('change', '#id_unidad_territorial_ubicacion_diferente', function (e) {
    e.preventDefault();
    var idUnidadTerritorial = $(this).val();
    var idTipoSede = $('#id_tipo_sede_ubicacion_diferente').val();
    if(idTipoSede=="2"){
        Tambos(null,"id_tambo_ubicacion_diferente",idTipoSede,idUnidadTerritorial);
    }else if(idTipoSede=="3"){
        Usuarios(null,"id_usuario_asignado_ubicacion_diferente",idTipoSede,idUnidadTerritorial);
    }
});

$('body').on('change', '#id_tambo_ubicacion_diferente', function (e) {
    e.preventDefault();
    var idTambo = $(this).val();
    var idTipoSede = $('#id_tipo_sede_ubicacion_diferente').val();
    Usuarios(null,"id_usuario_asignado_ubicacion_diferente",idTipoSede,idTambo);
});



async function UnidadesTerritoriales(idUnidadTerritorial,identificador){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/sede/get-lista-unidades-territoriales",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsUnidadesTerritoriales = "<option value>Seleccionar</option>";

                        $.each(results.unidadesTerritoriales, function( index, value ) {
                            optionsUnidadesTerritoriales = optionsUnidadesTerritoriales + `<option value='${value.id}'>${value.descripcion}</option>`;
                        });

                        $("#" + identificador).html(optionsUnidadesTerritoriales);

                        if(idUnidadTerritorial){
                            $("#" + identificador).val(idUnidadTerritorial);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function UnidadesOrganicas(idUnidadOrganica,identificador){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/sede/get-lista-unidades-organicas",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsUnidadesOrganicas = "<option value>Seleccionar</option>";

                        $.each(results.unidadesOrganicas, function( index, value ) {
                            optionsUnidadesOrganicas = optionsUnidadesOrganicas + `<option value='${value.id}'>${value.descripcion}</option>`;
                        });

                        $("#" + identificador).html(optionsUnidadesOrganicas);

                        if(idUnidadOrganica){
                            $("#" + identificador).val(idUnidadOrganica);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function Usuarios(idUsuario,identificador,idTipoSede,idPadre){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/usuario/get-lista-usuarios",
                method: "POST",
                data:{_csrf:csrf,idTipoSede:idTipoSede,idPadre:idPadre},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsUsuarios = "<option value>Seleccionar</option>";

                        $.each(results.usuarios, function( index, value ) {
                            optionsUsuarios = optionsUsuarios + `<option value='${value.id}'>${value.nombres}</option>`;
                        });

                        $("#" + identificador).html(optionsUsuarios);

                        if(idUsuario){
                            $("#" + identificador).val(idUsuario);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}


async function Tambos(idTambo,identificador,idTipoSede,idUnidadTerritorial){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/sede/get-lista-tambos",
                method: "POST",
                data:{_csrf:csrf,idTipoSede:idTipoSede,idUnidadTerritorial:idUnidadTerritorial},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsTambos = "<option value>Seleccionar</option>";

                        $.each(results.tambos, function( index, value ) {
                            optionsTambos = optionsTambos + `<option value='${value.id}'>${value.descripcion}</option>`;
                        });

                        $("#" + identificador).html(optionsTambos);

                        if(idTambo){
                            $("#" + identificador).val(idTambo);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}


//grabar asignacion

$('body').on('click', '.btn-grabar-asignacion', function (e) {

    e.preventDefault();
    e.preventDefault();
    var form = $('#formEquipoInformaticoAsignacion');
    var formData = $('#formEquipoInformaticoAsignacion').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                ListaAsignaciones();
                $('#modal').modal('hide');
            }
        },
    });
});



//modificar asignacion
$('body').on('click', '.btn-modificar-asignacion', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var idTipoSede = $(this).attr('data-idTipoSede');
    var idUnidadOrganica = $(this).attr('data-idUnidadOrganica');
    var idUnidadTerritorial = $(this).attr('data-idUnidadTerritorial');
    var idTambo = $(this).attr('data-idTambo');
    var idUsuarioAsignado = $(this).attr('data-idUsuarioAsignado');

    var idTipoSedeUbicacionDiferente = $(this).attr('data-idTipoSedeUbicacionDiferente');
    var idUnidadOrganicaUbicacionDiferente = $(this).attr('data-idUnidadOrganicaUbicacionDiferente');
    var idUnidadTerritorialUbicacionDiferente = $(this).attr('data-idUnidadTerritorialUbicacionDiferente');
    var idTamboUbicacionDiferente = $(this).attr('data-idTamboUbicacionDiferente');
    var idUsuarioAsignadoUbicacionDiferente = $(this).attr('data-idUsuarioAsignadoUbicacionDiferente');

    var flgUbicacionDiferente = $(this).attr('data-flgUbicacionDiferente');
    var fechaAsignacion = $(this).attr('data-fechaAsignacion');

    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/asignacion/update?id='+id,function(){
        $("#id_tipo_sede").val(idTipoSede);
        if(idTipoSede=="1"){
            $('.sede_central').show();
            UnidadesOrganicas(idUnidadOrganica,"id_unidad_organica");
            Usuarios(idUsuarioAsignado,"id_usuario_asignado",idTipoSede,idUnidadOrganica)
        }else if(idTipoSede=="2"){
            $('.tambo').show();
            UnidadesTerritoriales(idUnidadTerritorial,"id_unidad_territorial");
            Tambos(idTambo,"id_tambo",idTipoSede,idUnidadTerritorial);
            Usuarios(idUsuarioAsignado,"id_usuario_asignado",idTipoSede,idTambo)

        }else if(idTipoSede=="3"){
            $('.unidad_territorial').show();
            UnidadesTerritoriales(idUnidadTerritorial,"id_unidad_territorial");
            Usuarios(idUsuarioAsignado,"id_usuario_asignado",idTipoSede,idUnidadTerritorial)
        }

        if(flgUbicacionDiferente=="1"){
            $("#flg_ubicacion_diferente").prop("checked", true);
            $(".ubicacion_diferente").show()
        }


        $("#id_tipo_sede_ubicacion_diferente").val(idTipoSedeUbicacionDiferente);
        if(idTipoSedeUbicacionDiferente=="1"){
            $('.sede_central_ubicacion_diferente').show();
            UnidadesOrganicas(idUnidadOrganicaUbicacionDiferente,"id_unidad_organica_ubicacion_diferente");
            Usuarios(idUsuarioAsignadoUbicacionDiferente,"id_usuario_asignado_ubicacion_diferente",idTipoSedeUbicacionDiferente,idUnidadOrganicaUbicacionDiferente)
        }else if(idTipoSedeUbicacionDiferente=="2"){
            $('.tambo_ubicacion_diferente').show();
            UnidadesTerritoriales(idUnidadTerritorial,"id_unidad_territorial_ubicacion_diferente");
            Tambos(idTamboUbicacionDiferente,"id_tambo_ubicacion_diferente",idTipoSedeUbicacionDiferente,idUnidadTerritorialUbicacionDiferente);
            Usuarios(idUsuarioAsignadoUbicacionDiferente,"id_usuario_asignado_ubicacion_diferente",idTipoSedeUbicacionDiferente,idTamboUbicacionDiferente)

        }else if(idTipoSedeUbicacionDiferente=="3"){
            $('.unidad_territorial_ubicacion_diferente').show();
            UnidadesTerritoriales(idUnidadTerritorialUbicacionDiferente,"id_unidad_territorial_ubicacion_diferente");
            Usuarios(idUsuarioAsignadoUbicacionDiferente,"id_usuario_asignado_ubicacion_diferente",idTipoSedeUbicacionDiferente,idUnidadTerritorialUbicacionDiferente)
        }
        $("#fecha_asignacion").val(fechaAsignacion);
        

    });
    $('#modal').modal('show');
});
</script>