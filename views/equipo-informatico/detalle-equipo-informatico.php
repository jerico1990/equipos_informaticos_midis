
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <div class="row">
                <div class="col-12">
                    <h4 class="mb-0 font-size-18 align-middle">Equipo Informatico</h4>
                </div>
            </div>
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Detalle</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="">Estado:</label> <span class="info_estado"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Código patrimonial:</label> <span class="info_codigo_patrimonial"></span>
                    </div>
                    <div class="col-md-6">
                        <label for="">Denominación:</label> <span class="info_denominacion"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Fecha de ingreso:</label> <span class="info_fecha_ingreso"></span>
                    </div>
                    <div class="col-md-6">
                        <label for="">Tipo de equipo:</label> <span class="info_tipo_equipo"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Marca:</label> <span class="info_marca"></span>
                    </div>
                    <div class="col-md-6">
                        <label for="">Modelo:</label> <span class="info_modelo"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Color:</label> <span class="info_color"></span>
                    </div>
                    <div class="col-md-6">
                        <label for="">Nro serie:</label> <span class="info_nro_serie"></span>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->


<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Lista de mantenimientos</b>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="button" class="btn btn-success btn-agregar-mantenimiento float-right">Agregar</button>
                        </div>
                    </div>
                </div>
                <hr>
                <table id="lista-mantenimientos" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Tipo</th>
                            <th>Descripción</th>
                            <th>Responsable</th>
                            <th>Informe</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<!-- <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Lista de Programaciones</b>
                        </div>
                    </div>
                </div>
                <hr>
                <table id="lista-programaciones" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Fecha de programación</th>
                            <th>Se ejecuto</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> </div> <!-- end col -->



<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Lista de tickets</b>
                            
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <button class="btn btn-success btn-agregar-extension-ticket float-right">Agregar</button>
                        </div>
                    </div> -->
                </div>
                <hr>
                <table id="lista-tickets" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Tickets</th>
                            <th>Asignado</th>
                            <th>Materiales</th>
                            <th>Repuestos</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <b>Lista de asignaciones</b>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button class="btn btn-success btn-agregar-asignacion float-right">Agregar</button>
                        </div>
                    </div>
                </div>
                <hr>
                <table id="lista-asignaciones" class="table table-bordered dt-responsive ">
                    <thead>
                        <tr>
                            <th>Fecha de asignacion</th>
                            <!-- <th>Tipo sede</th>-->
                            <th>Ubicación</th> 
                            <th>Usuario</th>
                            <!-- <th>Ubicación diferente</th>
                            <th>Tipo sede</th>
                            <th>Ubicación</th>
                            <th>Usuario</th> -->
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>
<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var loading =   $('#staticBackdrop');
var idEquipoInformatico = "<?= $id ?>";
var configuracionMultiselect = {
                    selectAllText:' Todos',
                    //allSelectedText: 'Todos',
                    enableClickableOptGroups: true,
                    enableCollapsibleOptGroups: true,
                    enableFiltering: true,
                    includeSelectAllOption: false,
                    enableCaseInsensitiveFiltering: true,
                    filterPlaceholder: 'Búsqueda',
                    buttonClass : "col-md-12 form-control",
                    maxHeight: 200,
                    onInitialized: function(select, container) {
                        
                    },
                    buttonText: function(options) {
                        if (options.length === 0) {
                            return 'Ningún seleccionado';
                        }
                        else if (options.length > 3) {
                            return options.length + ' seleccionados';
                        }
                        else {
                            var selected = [];
                            options.each(function() {
                                selected.push([$(this).text(), $(this).data('order')]);
                            });

                            selected.sort(function(a, b) {
                                return a[1] - b[1];
                            });

                            var text = '';
                            for (var i = 0; i < selected.length; i++) {
                                text += selected[i][0] + ', ';
                            }

                            return text.substr(0, text.length -2);
                        }
                    }
                };

$('#lista-mantenimientos').DataTable();
$('#lista-programaciones').DataTable();
$('#lista-tickets').DataTable();
$('#lista-asignaciones').DataTable();

ListaMantenimientos();
/* ListaProgramaciones(); */
ListaTickets();
ListaAsignaciones();
InformacionGeneral(idEquipoInformatico);

async function InformacionGeneral(idEquipoInformatico){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/equipo-informatico/get-informacion-general",
                method: "POST",
                data:{_csrf:csrf,idEquipoInformatico:idEquipoInformatico},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var flg_activo="Inactivo";
                        if(results.informacionGeneral.flg_activo==1){
                            flg_activo = "Activo";
                        }

                        $('.info_estado').html(flg_activo);
                        $('.info_codigo_patrimonial').html(results.informacionGeneral.codigo_patrimonial);
                        $('.info_fecha_ingreso').html(results.informacionGeneral.fec_ingreso);
                        $('.info_marca').html(results.informacionGeneral.descripcion_marca);
                        $('.info_color').html(results.informacionGeneral.color);
                        $('.info_denominacion').html(results.informacionGeneral.descripcion_equipo_informatico);
                        $('.info_tipo_equipo').html(results.informacionGeneral.descripcion_tipo_equipo_informatico);
                        $('.info_modelo').html(results.informacionGeneral.descripcion_modelo);
                        $('.info_nro_serie').html(results.informacionGeneral.serie);

                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function ListaMantenimientos(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/mantenimiento/get-lista-mantenimientos',
                method: 'POST',
                data:{_csrf:csrf,idEquipoInformatico:idEquipoInformatico},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var mantenimientos ="";
                        $('#lista-mantenimientos').DataTable().destroy();
                        $.each(results.mantenimientos, function( index, value ) {
                            nombres_apellidos = (value.nombres_apellidos)?value.nombres_apellidos:"";
                            mantenimientos = mantenimientos + "<tr>";
                                mantenimientos = mantenimientos + "<td>" + ((value.fecha_mantenimiento)?value.fecha_mantenimiento:"") + "</td>";
                                mantenimientos = mantenimientos + "<td>" + ((value.descripcion_tipo_mantenimiento)?value.descripcion_tipo_mantenimiento:"") + "</td>";
                                mantenimientos = mantenimientos + "<td>" + ((value.observacion)?value.observacion:"") + "</td>";
                                mantenimientos = mantenimientos + "<td>" + nombres_apellidos + "</td>";
                                mantenimientos = mantenimientos + "<td>" + ((value.informe_tecnico)?"<a target='_blank' class='btn btn-success' href='<?= \Yii::$app->request->BaseUrl ?>/mantenimientoArchivo/" + value.ruta_informe_tecnico + "'>Descargar</a>":"") + "</td>";

                                mantenimientos = mantenimientos + "<td>";
                                    mantenimientos = mantenimientos + '<button data-id="' + value.id_equipo_informatico_mantenimiento + '" data-idResponsable="' + value.id_usuario_responsable + '" data-idTipoMantenimiento="' + value.id_tipo_mantenimiento + '" data-rutaInformeTecnico="' + value.ruta_informe_tecnico + '" class="btn btn-info btn-sm btn-modificar-mantenimiento" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    mantenimientos = mantenimientos + '<button data-id="' + value.id_equipo_informatico_mantenimiento + '" class="btn btn-danger btn-sm btn-eliminar-mantenimiento" href="#"><i class="fas fa-trash"></i></button>';

                                mantenimientos = mantenimientos +"</td>";
                            mantenimientos = mantenimientos + "</tr>";
                        });
                        
                        $('#lista-mantenimientos tbody').html(mantenimientos);
                        $('#lista-mantenimientos').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal('hide'); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function ListaProgramaciones(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/programacion-preventiva-equipo-informatico/get-lista-programaciones',
                method: 'POST',
                data:{_csrf:csrf},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var programacionesPreventivas ="";
                        $('#lista-programaciones').DataTable().destroy();
                        $.each(results.programacionesPreventivas, function( index, value ) {
                            
                            programacionesPreventivas = programacionesPreventivas + "<tr>";
                                programacionesPreventivas = programacionesPreventivas + "<td>" + ((value.fecha_programacion)?value.fecha_programacion:"") + "</td>";
                                programacionesPreventivas = programacionesPreventivas + "<td>" + ((value.flg_ejecuto_revision)?value.flg_ejecuto_revision:"") + "</td>";

                                programacionesPreventivas = programacionesPreventivas + "<td>";
                                    programacionesPreventivas = programacionesPreventivas + '<button data-id="' + value.id_programacion_preventiva_detalle + '" class="btn btn-info btn-sm btn-modificar-programacion" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    programacionesPreventivas = programacionesPreventivas + '<button data-id="' + value.id_programacion_preventiva_detalle + '" class="btn btn-danger btn-sm btn-eliminar-programacion" href="#"><i class="fas fa-trash"></i></button>';

                                    
                                programacionesPreventivas = programacionesPreventivas +"</td>";
                            programacionesPreventivas = programacionesPreventivas + "</tr>";
                        });
                        
                        $('#lista-programaciones tbody').html(programacionesPreventivas);
                        $('#lista-programaciones').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal('hide'); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function ListaTickets(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/ticket/get-lista-tickets',
                method: 'POST',
                data:{_csrf:csrf,idEquipoInformatico:idEquipoInformatico},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var tickets ="";
                        $('#lista-tickets').DataTable().destroy();
                        $.each(results.tickets, function( index, value ) {
                            
                            tickets = tickets + "<tr>";
                                tickets = tickets + "<td>" + ((value.id_ticket)?value.id_ticket:"") + "</td>";
                                tickets = tickets + "<td>" + ((value.usuario_asignado)?value.usuario_asignado:"") + "</td>";  
                                tickets = tickets + "<td>" + ((value.material)?value.material:"") + "</td>";  
                                tickets = tickets + "<td>" + ((value.repuesto)?value.repuesto:"") + "</td>";  

                                tickets = tickets + "<td>";
                                    tickets = tickets + '<button data-idEquipoInformaticoTicket="' + value.id_equipo_informatico_ticket + '" data-idTicket="' + value.id_ticket + '"  class="btn btn-info btn-sm btn-modificar-ticket" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                    tickets = tickets + '<button data-idEquipoInformaticoTicket="' + value.id_equipo_informatico_ticket + '" data-idTicket="' + value.id_ticket + '" class="btn btn-danger btn-sm btn-eliminar-ticket" href="#"><i class="fas fa-trash"></i></button>';

                                tickets = tickets +"</td>";
                            tickets = tickets + "</tr>";
                        });
                        
                        $('#lista-tickets tbody').html(tickets);
                        $('#lista-tickets').DataTable({
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal('hide'); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

async function ListaAsignaciones(){
    await   $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/asignacion/get-lista-asignaciones',
                method: 'POST',
                data:{_csrf:csrf,idEquipoInformatico:idEquipoInformatico},
                dataType:'Json',
                beforeSend:function(){
                    loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var asignaciones ="";
                        $('#lista-asignaciones').DataTable().destroy();
                        $.each(results.asignaciones, function( index, value ) {
                            asignaciones = asignaciones + "<tr>";
                                asignaciones = asignaciones + "<td>" + ((value.fecha_asignacion)?value.fecha_asignacion:"") + "</td>";
                                asignaciones = asignaciones + "<td>" + ((value.ubicacion)?value.ubicacion:"") + "</td>";
                                asignaciones = asignaciones + "<td>" + ((value.empleado)?value.empleado:"") + "</td>";
                                asignaciones = asignaciones + "<td>" + ((value.flg_activo=="1")?"Activo":"Desactivo") + "</td>";

                                asignaciones = asignaciones + "<td>";
                                    
                                    if(value.flg_activo==1){
                                        asignaciones = asignaciones + '<button data-id="' + value.id_equipo_informatico_asignacion + '" data-flgActivo="' + value.flg_activo + '" data-idUbicacion="' + value.id_ubicacion + '" data-idEmpleado="' + value.id_empleado + '"  data-idUsuarioAsignado="' + value.id_usuario_asignado + '"  data-fechaAsignacion="' + value.fecha_asignacion + '" class="btn btn-info btn-sm btn-modificar-asignacion" href="#"><i class="fas fa-pencil-alt"></i></button> ';
                                        asignaciones = asignaciones + '<button data-id="' + value.id_equipo_informatico_asignacion + '" class="btn btn-danger btn-sm btn-eliminar-asignacion" href="#"><i class="fas fa-trash"></i></button>';
                                    }
                                    

                                    
                                asignaciones = asignaciones +"</td>";
                            asignaciones = asignaciones + "</tr>";
                        });
                        
                        $('#lista-asignaciones tbody').html(asignaciones);
                        $('#lista-asignaciones').DataTable({
                            "order": [[ 3, "asc" ]],
                            "paging": true,
                            "lengthChange": true,
                            "searching": false,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "pageLength" : 5,
                            "language": {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningun dato disponible en esta lista",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                        });

                        setTimeout(function(){ loading.modal('hide'); }, 2000);
                        
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso.');
                }
            });
}

/* Mantenimiento */

async function MantenimientoResponsables(idResponsable){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/usuario/get-lista-responsables",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsResponsable = "<option value>Seleccionar</option>";

                        $.each(results.responsables, function( index, value ) {
                            optionsResponsable = optionsResponsable + `<option value='${value.id_empleado}'>${value.NOMBRE_APELLIDOS}</option>`;
                        });

                        $("#equipo-informatico-mantenimiento-id_usuario_responsable").html(optionsResponsable);

                        if(idResponsable){
                            $("#equipo-informatico-mantenimiento-id_usuario_responsable").val(idResponsable);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

async function MantenimientoTipos(idTipoMantenimiento){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/tipo-mantenimiento/get-lista-tipos-mantenimiento",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsTiposMantenimiento = "<option value>Seleccionar</option>";

                        $.each(results.tiposMantenimiento, function( index, value ) {
                            optionsTiposMantenimiento = optionsTiposMantenimiento + `<option value='${value.id_tipo_mantenimiento}'>${value.descripcion_tipo_mantenimiento}</option>`;
                        });

                        $("#equipo-informatico-mantenimiento-id_tipo_mantenimiento").html(optionsTiposMantenimiento);

                        if(idTipoMantenimiento){
                            $("#equipo-informatico-mantenimiento-id_tipo_mantenimiento").val(idTipoMantenimiento);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

/* Asignaciones */

async function AsignacionUbicaciones(idUbicacion){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/ubicacion/get-lista-ubicaciones",
                method: "POST",
                data:{_csrf:csrf},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsUbicaciones = "<option value>Seleccionar</option>";

                        $.each(results.ubicaciones, function( index, value ) {
                            optionsUbicaciones = optionsUbicaciones + `<option value='${value.id_ubicacion}'>${value.ubicacion}</option>`;
                        });

                        $("#equipo-informatico-asignacion-id_ubicacion").html(optionsUbicaciones);
                        
                        if(idUbicacion){
                            $("#equipo-informatico-asignacion-id_ubicacion").val(idUbicacion);
                        }

                        $('#equipo-informatico-asignacion-id_ubicacion').multiselect(configuracionMultiselect);
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}


async function AsignacionResponsables(idUbicacion,idResponsable){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/usuario/get-lista-responsables-asignacion",
                method: "POST",
                data:{_csrf:csrf,idUbicacion:idUbicacion},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        $('#equipo-informatico-asignacion-id_usuario_asignado').multiselect('destroy');
                        var optionsResponsable = "<option value>Seleccionar</option>";

                        $.each(results.responsables, function( index, value ) {
                            optionsResponsable = optionsResponsable + `<option value='${value.id_empleado}'>${value.empleado}</option>`;
                        });

                        $("#equipo-informatico-asignacion-id_usuario_asignado").html(optionsResponsable);

                        if(idResponsable){
                            $("#equipo-informatico-asignacion-id_usuario_asignado").val(idResponsable);
                        }

                        $('#equipo-informatico-asignacion-id_usuario_asignado').multiselect(configuracionMultiselect);
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}

//agregar mantenimiento

$('body').on('click', '.btn-agregar-mantenimiento', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/mantenimiento/create?idEquipoInformatico='+idEquipoInformatico,function(){
        MantenimientoResponsables()
        MantenimientoTipos()
    });
    $('#modal').modal('show');
});

//modificar mantenimiento
$('body').on('click', '.btn-modificar-mantenimiento', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var idResponsable = $(this).attr('data-idResponsable');
    var idTipoMantenimiento = $(this).attr('data-idTipoMantenimiento');
    var rutaInformeTecnico = $(this).attr('data-rutaInformeTecnico');
    
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/mantenimiento/update?id='+id,function(){
        MantenimientoResponsables(idResponsable);
        MantenimientoTipos(idTipoMantenimiento);
        if(rutaInformeTecnico){
            $('#equipo-informatico-mantenimiento-archivo').hide();
            $('.archivo-mantenimiento').show();
        }
    });
    $('#modal').modal('show');
});

$('body').on('click', '.btn-eliminar-mantenimiento', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/mantenimiento/delete",
        method: "POST",
        data:{_csrf:csrf,id:id},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                ListaMantenimientos()
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
});



$('body').on('click', '.btn-eliminar-archivo-mantenimiento', function (e) {
    e.preventDefault();
    $('#equipo-informatico-mantenimiento-archivo').show();
    $('.archivo-mantenimiento').hide();
});
//grabar mantenimiento

$('body').on('click', '.btn-grabar-mantenimiento', function (e) {

    e.preventDefault();
    var form = $('#formEquipoInformaticoMantenimiento');
    var formData = new FormData();
	formData.append("_csrf", csrf);
    formData.append("EquipoInformaticoMantenimiento[fecha_mantenimiento]", $('#equipo-informatico-mantenimiento-fecha_mantenimiento').val());
    formData.append("EquipoInformaticoMantenimiento[id_tipo_mantenimiento]", $('#equipo-informatico-mantenimiento-id_tipo_mantenimiento').val());
    formData.append("EquipoInformaticoMantenimiento[id_usuario_responsable]", $('#equipo-informatico-mantenimiento-id_usuario_responsable').val());
    formData.append("EquipoInformaticoMantenimiento[archivo]", document.getElementById('equipo-informatico-mantenimiento-archivo').files[0]);
    formData.append("EquipoInformaticoMantenimiento[observacion]", $('#equipo-informatico-mantenimiento-observacion').val());
 

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        cache: false,
		contentType: false,
		processData: false,
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                ListaMantenimientos();
                $('#modal').modal('hide');
            }
        },
    });
});

/* Ticket */

//agregar Ticket
$('body').on('click', '.btn-agregar-extension-ticket', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/ticket/create?idEquipoInformatico='+idEquipoInformatico+'&idTicket=1');
    $('#modal').modal('show');
});

//modificar ticket
$('body').on('click', '.btn-modificar-ticket', function (e) {
    e.preventDefault();
    var idEquipoInformaticoTicket = $(this).attr('data-idEquipoInformaticoTicket');
    var idTicket = $(this).attr('data-idTicket');
    console.log(idEquipoInformaticoTicket);
    if(idEquipoInformaticoTicket!='null'){
        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/ticket/update?idEquipoInformaticoTicket='+idEquipoInformaticoTicket);
    }else{
        $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/ticket/create?idTicket='+idTicket+'&idEquipoInformatico='+idEquipoInformatico);
    }
    
    $('#modal').modal('show');
});


$('body').on('click', '.btn-eliminar-ticket', function (e) {
    e.preventDefault();
    var idEquipoInformaticoTicket = $(this).attr('data-idEquipoInformaticoTicket');
    $.ajax({
        url: "<?= \Yii::$app->request->BaseUrl ?>/ticket/delete",
        method: "POST",
        data:{_csrf:csrf,idEquipoInformaticoTicket:idEquipoInformaticoTicket},
        dataType:"Json",
        beforeSend:function(xhr, settings)
        {
            //loading.modal("show");
        },
        success:function(results)
        {   
            if(results && results.success){
                ListaTickets()
            }
        },
        error:function(){
            alert("Error al realizar el proceso.");
        }
    });
});

//grabar Ticket

$('body').on('click', '.btn-grabar-ticket', function (e) {

    e.preventDefault();
    e.preventDefault();
    var form = $('#formEquipoInformaticoTicket');
    var formData = $('#formEquipoInformaticoTicket').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                ListaTickets();
                $('#modal').modal('hide');
            }
        },
    });
});


/* asingnacion */


$('body').on('click', '.btn-agregar-asignacion', function (e) {
    e.preventDefault();
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/asignacion/create?idEquipoInformatico='+idEquipoInformatico,function(){
        $('#equipo-informatico-asignacion-id_usuario_asignado').multiselect(configuracionMultiselect);
        AsignacionUbicaciones();
    });
    $('#modal').modal('show');
});

$('body').on('change', '#equipo-informatico-asignacion-id_ubicacion', function (e) {
    e.preventDefault();
    var idUbicacion = $(this).val();
    AsignacionResponsables(idUbicacion)
});

async function Usuarios(idUsuario,identificador,idTipoSede,idPadre){
    await   $.ajax({
                url: "<?= \Yii::$app->request->BaseUrl ?>/usuario/get-lista-usuarios",
                method: "POST",
                data:{_csrf:csrf,idTipoSede:idTipoSede,idPadre:idPadre},
                dataType:"Json",
                beforeSend:function(xhr, settings)
                {
                    //loading.modal("show");
                },
                success:function(results)
                {   
                    if(results && results.success){
                        var optionsUsuarios = "<option value>Seleccionar</option>";

                        $.each(results.usuarios, function( index, value ) {
                            optionsUsuarios = optionsUsuarios + `<option value='${value.id}'>${value.nombres}</option>`;
                        });

                        $("#" + identificador).html(optionsUsuarios);

                        if(idUsuario){
                            $("#" + identificador).val(idUsuario);
                        }
                    }
                },
                error:function(){
                    alert("Error al realizar el proceso.");
                }
            });
}



//grabar asignacion

$('body').on('click', '.btn-grabar-asignacion', function (e) {

    e.preventDefault();
    e.preventDefault();
    var form = $('#formEquipoInformaticoAsignacion');
    var formData = $('#formEquipoInformaticoAsignacion').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }

    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        beforeSend:function()
        {
            loading.modal("show");
        },
        success: function (results) {
            if(results.success){
                ListaAsignaciones();
                $('#modal').modal('hide');
            }
        },
    });
});



//modificar asignacion
$('body').on('click', '.btn-modificar-asignacion', function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var idUsuarioAsignado = $(this).attr('data-idUsuarioAsignado');
    var fechaAsignacion = $(this).attr('data-fechaAsignacion');
    var flgActivo = $(this).attr('data-flgActivo');
    var idUbicacion = $(this).attr('data-idUbicacion');

    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/asignacion/update?id='+id,function(){
        AsignacionUbicaciones(idUbicacion);
        AsignacionResponsables(idUbicacion,idUsuarioAsignado)
        $("#fecha_asignacion").val(fechaAsignacion);
        $("#flg_activo").val(flgActivo);
    });
    $('#modal').modal('show');
});
</script>