<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formEquipoInformatico','class' => 'form-horizontal']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Codigo patrimonial:</label>
                    <input type="text" id="equipo-informatico-codigo_patrimonial" class="form-control" name="EquipoInformatico[codigo_patrimonial]" value="<?= $model->codigo_patrimonial ?>">
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label>Denominación:</label>
                    <input type="text" id="equipo-informatico-descripcion_equipo_informatico" class="form-control" name="EquipoInformatico[descripcion_equipo_informatico]" value="<?= $model->descripcion_equipo_informatico ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Marca:</label>
                    <select id="equipo-informatico-id_marca" class="form-control" name="EquipoInformatico[id_marca]">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Modelo:</label>
                    <select id="equipo-informatico-id_modelo" class="form-control" name="EquipoInformatico[id_modelo]">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Tipo:</label>
                    <select id="equipo-informatico-id_tipo_equipo_informatico" class="form-control" name="EquipoInformatico[id_tipo_equipo_informatico]">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Color:</label>
                    <input type="text" id="equipo-informatico-color" class="form-control" name="EquipoInformatico[color]" value="<?= $model->color ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Serie:</label>
                    <input type="text" id="equipo-informatico-serie" class="form-control" name="EquipoInformatico[serie]" value="<?= $model->serie ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Proveedor de Garantia:</label>
                    <input type="text" id="equipo-proveedor_garantia-color" class="form-control" name="EquipoInformatico[proveedor_garantia]" value="<?= $model->proveedor_garantia ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Fecha Fin Garantia:</label>
                    <input type="date" id="equipo-informatico-fec_fin_garantia_proveedor" class="form-control" name="EquipoInformatico[fec_fin_garantia_proveedor]" value="<?= $model->fec_fin_garantia_proveedor ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Telefono del proveedor:</label>
                    <input type="text" id="equipo-informatico-proveedor_telefono" class="form-control" name="EquipoInformatico[proveedor_telefono]" value="<?= $model->proveedor_telefono ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Fecha de ingreso:</label>
                    <input type="date" id="equipo-proveedor_garantia-fec_ingreso" class="form-control" name="EquipoInformatico[fec_ingreso]" value="<?= $model->fec_ingreso ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Estado:</label>
                    <select id="equipo-proveedor_garantia-flg_activo" class="form-control" name="EquipoInformatico[flg_activo]">
                        <option value>Seleccionar</option>
                        <option value="1" <?= (($model->flg_activo=="1")?"selected":"") ?> >Activo</option>
                        <option value="2" <?= (($model->flg_activo=="2")?"selected":"") ?> >Desactivo</option>
                    </select>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success btn-grabar-equipo-informatico">Grabar</button>
    </div>

<?php ActiveForm::end(); ?>