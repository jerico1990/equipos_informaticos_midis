<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formEquipoInformaticoTicket','class' => 'form-horizontal']]); ?>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
       
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Materiales:</label>
                    <textarea  id="equipo-informatico-ticket-material" class="form-control" name="EquipoInformaticoTicket[material]" cols="30" rows="5"><?= $model->material ?></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Repuestos:</label>
                    <textarea  id="equipo-informatico-ticket-repuesto" class="form-control" name="EquipoInformaticoTicket[repuesto]" cols="30" rows="5"><?= $model->repuesto ?></textarea>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success btn-grabar-ticket">Grabar</button>
    </div>
</div>
<?php ActiveForm::end(); ?>