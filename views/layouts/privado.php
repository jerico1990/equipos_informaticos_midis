<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>MIDIS | Administrador</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->


    <!-- Bootstrap Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/app.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom Css-->
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/css/custom.css" rel="stylesheet" type="text/css" />
    <!-- JAVASCRIPT -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jquery/jquery.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/metismenu/metisMenu.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/simplebar/simplebar.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/node-waves/waves.min.js"></script>

    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />


    

    

    
    <!-- Required datatable js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/jszip/jszip.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>

    <!-- Responsive examples -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script src="https://cdn.rawgit.com/kevindb/jquery-load-json/v1.3.4/dist/jquery.loadJSON.min.js" integrity="sha384-ivtX4sn4dcdfHiO4e0/956wIQSerxsy2QZ6EHzdCVLlyGYYjSb8bqdxKY8IsfDGh" crossorigin="anonymous"></script>

    <script type="text/javascript" src="<?= \Yii::$app->request->BaseUrl ?>/bootstrap-multiselect-master/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/bootstrap-multiselect-master/css/bootstrap-multiselect.css" type="text/css"/>

    <!-- <link href="<?= \Yii::$app->request->BaseUrl ?>/css/pagination.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= \Yii::$app->request->BaseUrl ?>/js/pagination.min.js"></script> -->

    <script>
	$.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
	</script>

<style>
.multiselect-native-select .btn-group,.multiselect-native-select .btn-group .multiselect {
    width: 100% !important;
}

.dropdown-menu.show{
    width:100%;
}
</style>
</head>

<body data-sidebar="dark">
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog text-center" role="document">
            Cargando <br>
            <div class="spinner-border text-primary m-1" role="status"></div>
        </div>
    </div>
    <!-- Begin page -->
    <div id="layout-wrapper">

        
      <!--   <div class="vertical-menu">

            <div data-simplebar class="h-100">

                <div id="sidebar-menu">
                    <ul class="metismenu list-unstyled" id="side-menu">
                        <li class="menu-title">Menu</li>

                    
                        <li>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/equipo-informatico" class=" waves-effect">
                                <i class="bx bx-collection"></i>
                                <span>Equipos informaticos</span>
                            </a>
                        </li>
                        

                    </ul>
                </div>
               
            </div>
        </div> -->
        <!-- Left Sidebar End -->

        <div class="" id="result">
            <div class="page-content">
                <div class="container-fluid">
                    <?= $content ?>
                </div>
            </div>
        </div>

    </div>
    <!-- END layout-wrapper -->

    
    <!-- /Right-bar -->
    <!-- Right bar overlay-->
    <script>
    
    $('.numerico').keypress(function (tecla) {
        var reg = /^[0-9.\s]+$/;
        if(!reg.test(String.fromCharCode(tecla.which))){
            return false;
        }
        return true;
    });		
    $('.texto').keypress(function(tecla) {
        var reg = /^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ'_\s]+$/;
        if(!reg.test(String.fromCharCode(tecla.which))){
            return false;
        }
        return true;
    });
        
    </script>
    <!-- App js -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/skote/js/app.js"></script>
</body>

</html>