<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formEquipoInformaticoAsignacion','class' => 'form-horizontal']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
       
        <!-- <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Sede Central o Tambos o Unidad Territorial</label>
                    <select name="EquipoInformaticoAsignacion[id_tipo_sede]" id="id_tipo_sede" class="form-control">
                        <option value>Seleccionar</option>
                        <option value="1">Sede Central</option>
                        <option value="2">Tambo</option>
                        <option value="3">Unidad Territorial</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 tambo unidad_territorial" style="display:none">
                <div class="form-group">
                    <label>Unidad Territorial</label>
                    <select name="EquipoInformaticoAsignacion[id_unidad_territorial]" id="id_unidad_territorial" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 sede_central" style="display:none">
                <div class="form-group">
                    <label>Unidad Organica</label>
                    <select name="EquipoInformaticoAsignacion[id_unidad_organica]" id="id_unidad_organica" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 tambo" style="display:none">
                <div class="form-group">
                    <label>Tambos</label>
                    <select name="EquipoInformaticoAsignacion[id_tambo]" id="id_tambo" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 sede_central tambo unidad_territorial" style="display:none">
                <div class="form-group">
                    <label>Lista de Usuarios</label>
                    <select name="EquipoInformaticoAsignacion[id_usuario_asignado]" id="id_usuario_asignado" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div> -->

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Lista de Ubicaciones</label>
                    <select id="equipo-informatico-asignacion-id_ubicacion" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Lista de Usuarios</label>
                    <select name="EquipoInformaticoAsignacion[id_usuario_asignado]" id="equipo-informatico-asignacion-id_usuario_asignado" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div>
<!-- 


        <div class="row">
            <div class="col-md-12 ubicacion_diferente" style="display:none">
                <div class="form-group">
                    <label>Sede Central o Tambos o Unidad Territorial</label>
                    <select name="EquipoInformaticoAsignacion[id_tipo_sede_ubicacion_diferente]" id="id_tipo_sede_ubicacion_diferente" class="form-control">
                        <option value>Seleccionar</option>
                        <option value="1">Sede Central</option>
                        <option value="2">Tambo</option>
                        <option value="3">Unidad Territorial</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 tambo_ubicacion_diferente unidad_territorial_ubicacion_diferente" style="display:none">
                <div class="form-group">
                    <label>Unidad Territorial</label>
                    <select name="EquipoInformaticoAsignacion[id_unidad_territorial_ubicacion_diferente]" id="id_unidad_territorial_ubicacion_diferente" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 sede_central_ubicacion_diferente" style="display:none">
                <div class="form-group">
                    <label>Unidad Organica</label>
                    <select name="EquipoInformaticoAsignacion[id_unidad_organica_ubicacion_diferente]" id="id_unidad_organica_ubicacion_diferente" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 tambo_ubicacion_diferente" style="display:none">
                <div class="form-group">
                    <label>Tambos</label>
                    <select name="EquipoInformaticoAsignacion[id_tambo_ubicacion_diferente]" id="id_tambo_ubicacion_diferente" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 sede_central_ubicacion_diferente tambo_ubicacion_diferente unidad_territorial_ubicacion_diferente" style="display:none">
                <div class="form-group">
                    <label>Lista de Usuarios</label>
                    <select name="EquipoInformaticoAsignacion[id_usuario_asignado_ubicacion_diferente]" id="id_usuario_asignado_ubicacion_diferente" class="form-control">
                        <option value>Seleccionar</option>
                    </select>
                </div>
            </div>
        </div>
 -->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Fecha asignacion</label>
                    <input type="date" class="form-control" name="EquipoInformaticoAsignacion[fecha_asignacion]" id="fecha_asignacion">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Estado:</label>
                    <select id="flg_activo" class="form-control" name="EquipoInformaticoAsignacion[flg_activo]">
                        <option value>Seleccionar</option>
                        <option value="1">Activo</option>
                        <option value="2">Desactivo</option>
                    </select>
                </div>
            </div>
        </div>



    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success btn-grabar-asignacion">Grabar</button>
    </div>

<?php ActiveForm::end(); ?>